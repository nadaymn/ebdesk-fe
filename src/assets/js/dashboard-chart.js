var candidateChart = document
  .getElementById("newCandidateChart")
  .getContext("2d");
var newCandidateChart = new Chart(candidateChart, {
  type: "line",
  data: {
    labels: [
      " ",
      "17 Mar",
      "18 Mar",
      "19 Mar",
      "20 Mar",
      "21 Mar",
      "22 Mar",
      "23 Mar",
      "24 Mar",
      "25 Mar",
      "26 Mar",
      "27 Mar",
      "28 Mar",
      "29 Mar",
      "30 Mar",
    ],
    datasets: [
      {
        data: [11, 30, 39, 30, 43, 23, 50, 25, 41, 28, 30, 32, 37, 40, 0],
        label: "New Candidate",
        borderWidth: 2,
        pointBorderColor: "transparent",
        pointHoverBackgroundColor: "rgba(63,82,227,1)",
        borderColor: "#0F123F",
        backgroundColor: "#0f123f25",
      },
      {
        data: [, 0, 10, 12, 20, 40, 19, 20, 21, 25, 32, 22, 30, 55, 15],
        label: "On Progress",
        borderWidth: 2,
        borderColor: "#FF8988",
        pointBorderColor: "transparent",
        backgroundColor: "#ff8a884f",
      },
      {
        data: [],
        label: "On Progress",
        borderWidth: 2,
        borderColor: "#FF922B",
        pointBorderColor: "transparent",
        backgroundColor: "rgb(255,165,0,0.1)",
      },
      {
        data: [],
        label: "On Progress",
        borderWidth: 2,
        borderColor: "#3476F2",
        pointBorderColor: "transparent",
        backgroundColor: "#3477f225",
      },
    ],
  },
  options: {
    responsive: true,
    layout: {
      padding: {
        top: 10,
        left: 20,
        right: 30,
        bottom: 250,
      },
    },
    legend: {
      display: false,
    },
    scales: {
      yAxes: [
        {
          gridLines: {
            display: true,
            drawBorder: true,
          },
          ticks: {
            display: true,
            fontFamily: "Roboto",
            fontColor: "#A3A3A3",
          },
        },
      ],
      xAxes: [
        {
          gridLines: {
            drawBorder: true,
            display: true,
          },
          ticks: {
            display: true,
            fontFamily: "Roboto",
            fontColor: "#A3A3A3",
          },
        },
      ],
    },
  },
});

var pieChart = document.getElementById("pieChart").getContext("2d");

var xValues = ["Chart 1", "Chart 2", "Chart 3"];
var yValues = [70, 55, 30];
var barColors = ["#FF8988", "#0F123F", "#EFEFEF"];

new Chart("pieChart", {
  type: "doughnut",
  data: {
    labels: xValues,
    datasets: [
      {
        backgroundColor: barColors,
        data: yValues,
      },
    ],
  },
  options: {
    legend: {
      display: false,
    },
  },
});

var backEndChart = document.getElementById("backEndChart").getContext("2d");
var xValues = ["Chart 1", "Chart 2"];
var yValues = [50, 50];
var barColors = ["#EFEFEF", "#0F123F"];

new Chart("backEndChart", {
  type: "doughnut",
  data: {
    labels: xValues,
    datasets: [
      {
        backgroundColor: barColors,
        data: yValues,
      },
    ],
  },
  options: {
    cutoutPercentage: 75,
    responsive: true,
    legend: {
      display: false,
    },
    aspectRatio: 1,
  },
});

var marketingChart = document.getElementById("marketingChart").getContext("2d");
var xValues = ["Chart 1", "Chart 2"];
var yValues = [75, 25];
var barColors = ["#EFEFEF", "#0F123F"];

new Chart("marketingChart", {
  type: "doughnut",
  data: {
    labels: xValues,
    datasets: [
      {
        backgroundColor: barColors,
        data: yValues,
      },
    ],
  },
  options: {
    cutoutPercentage: 75,
    responsive: true,
    legend: {
      display: false,
    },
    aspectRatio: 1,
  },
});

var frontEndChart = document.getElementById("frontEndChart").getContext("2d");
var xValues = ["Chart 1", "Chart 2"];
var yValues = [25, 75];
var barColors = ["#EFEFEF", "#0F123F"];

new Chart("frontEndChart", {
  type: "doughnut",
  data: {
    labels: xValues,
    datasets: [
      {
        backgroundColor: barColors,
        data: yValues,
      },
    ],
  },
  options: {
    cutoutPercentage: 75,
    responsive: true,
    legend: {
      display: false,
    },
    aspectRatio: 1,
  },
});

var dataAnalystChart = document
  .getElementById("dataAnalystChart")
  .getContext("2d");
var xValues = ["Chart 1", "Chart 2"];
var yValues = [0, 100];
var barColors = ["#EFEFEF", "#0F123F"];

new Chart("dataAnalystChart", {
  type: "doughnut",
  data: {
    labels: xValues,
    datasets: [
      {
        backgroundColor: barColors,
        data: yValues,
      },
    ],
  },
  options: {
    cutoutPercentage: 75,
    responsive: true,
    legend: {
      display: false,
    },
    aspectRatio: 1,
  },
});
