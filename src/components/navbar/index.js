import React from "react";

import navbar from "../../assets/css/navbar.module.css";

import icon1 from "../../assets/img/dashboard/remix-icons/arrow-drop-down-line.svg";
import searchIcon from "../../assets/img/dashboard/search.svg";
import notifIcon from "../../assets/img/dashboard/remix-icons/notification-3-line.svg";
import userImg from "../../assets/img/dashboard/user.png";

export default function Navbar() {
  return (
    <div>
      <div className={navbar.navbar}>
        <div className={navbar.search_box}>
          <img src={icon1} className={navbar.icon_1} alt="" />
          <input type="text" placeholder="Search" />
          <img src={searchIcon} className={navbar.icon_2} alt="" />
        </div>
        <div className={navbar.bell_icon}>
          <div className={navbar.ellipses}></div>
          <img src={notifIcon} alt="" />
        </div>
        <div className={navbar.frame_5550}>
          <img src={icon1} className={navbar.icon} alt="" />
          <div className={navbar.frame_5549}>
            <p>Bayuna Rama</p>
            <img src={userImg} alt="" />
          </div>
        </div>
      </div>
    </div>
  );
}
