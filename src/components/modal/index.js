import modal from "../../assets/css/modal.module.css";
import utility from "../../assets/css/utility.module.css";

import saveIcon from "../../assets/img/account/save.svg";

const Modal = ({ setIsOpen, children }) => {
  return (
    <div className={`${modal.modal} ${modal.modal_hidden}`}>
      <div className={modal.modal_content}>
        <div className={modal.form_control}>{children}</div>

        <div className={`${modal.modal_footer} ${utility.mt_3}`}>
          <button
            onClick={() => setIsOpen(false)}
            className={modal.btn_outline_blue}
          >
            Cancel
          </button>
          <button className={modal.btn_dark_blue}>
            <img src={saveIcon} alt="" />
            <div className={utility.ml_1}>Save</div>
          </button>
        </div>

        <button
          type="button"
          onClick={() => setIsOpen(false)}
          className={modal.modal_close}
        >
          &times;
        </button>
      </div>
    </div>
  );
};

export default Modal;
