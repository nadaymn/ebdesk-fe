import React from "react";

import account from "../../../assets/css/account.module.css";
import utility from "../../../assets/css/utility.module.css";

import Row from "./row";

import arrowDown from "../../../assets/img/account/arrow-down.svg";
import arrowLeft from "../../../assets/img/account/arrow-left.svg";
import arrowRight from "../../../assets/img/account/arrow-right.svg";

export default function Table() {
  return (
    <div class={account.table_responsive}>
      <table class={`${account.table} ${account.table_striped}`}>
        <thead>
          <th width="200px">Username</th>
          <th width="250px">Phone Number</th>
          <th>Email</th>
          <th width="150px">Password</th>
          <th width="150px">Status</th>
          <th>Action</th>
        </thead>
        <tbody>
          <Row
            thumbUsername="A"
            username="adminrahasia123"
            phone="089672832132"
            email="ebdesk@gmail.com"
            password="Test123@"
            status="Active"
            checked={false}
          />
          <Row
            thumbUsername="B"
            username="adminrahasia123"
            phone="089672832132"
            email="ebdesk@gmail.com"
            password="Test123@"
            status="Disable"
            checked={false}
          />
          <Row
            thumbUsername="B"
            username="adminrahasia123"
            phone="089672832132"
            email="ebdesk@gmail.com"
            password="Test123@"
            status="Disable"
            checked={true}
          />
          <Row
            thumbUsername="A"
            username="adminrahasia123"
            phone="089672832132"
            email="ebdesk@gmail.com"
            password="Test123@"
            status="Active"
            checked={false}
          />
          <Row
            thumbUsername="A"
            username="adminrahasia123"
            phone="089672832132"
            email="ebdesk@gmail.com"
            password="Test123@"
            status="Active"
            checked={true}
          />
          <Row
            thumbUsername="B"
            username="adminrahasia123"
            phone="089672832132"
            email="ebdesk@gmail.com"
            password="Test123@"
            status="Active"
            checked={true}
          />
          <Row
            thumbUsername="A"
            username="adminrahasia123"
            phone="089672832132"
            email="ebdesk@gmail.com"
            password="Test123@"
            status="Disable"
            checked={false}
          />
          <Row
            thumbUsername="B"
            username="adminrahasia123"
            phone="089672832132"
            email="ebdesk@gmail.com"
            password="Test123@"
            status="Disable"
            checked={false}
          />
          <Row
            thumbUsername="A"
            username="adminrahasia123"
            phone="089672832132"
            email="ebdesk@gmail.com"
            password="Test123@"
            status="Disable"
            checked={true}
          />
          <Row
            thumbUsername="B"
            username="adminrahasia123"
            phone="089672832132"
            email="ebdesk@gmail.com"
            password="Test123@"
            status="Active"
            checked={true}
          />
        </tbody>
      </table>
      <div class={account.card_footer}>
        <div class={account.page_length}>
          <p>Rows per page: 10</p>
          <img src={arrowDown} class={utility.ml_1} alt="" />
        </div>
        <div class={account.page_num}>
          <p>1 - 6 of 6</p>
        </div>
        <div class={account.pagination}>
          <img src={arrowLeft} alt="" />
          <img src={arrowRight} class={utility.ml_5} alt="" />
        </div>
      </div>
    </div>
  );
}
