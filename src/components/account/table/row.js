import React from "react";
import account from "../../../assets/css/account.module.css";

import moreHorizontal from "../../../assets/img/dashboard/more-horizontal.svg";

export default function Row(props) {
  const thumbUsername = props.thumbUsername;
  let thumbUsernameEl = "";

  const status = props.status;
  let statusEl = "";

  if (thumbUsername === "A") {
    thumbUsernameEl = <div className={account.user_thumb_green}>A</div>;
  } else {
    thumbUsernameEl = <div className={account.user_thumb_purple}>B</div>;
  }

  if (status === "Active") {
    statusEl = <span className={account.badge_success}>Active</span>;
  } else {
    statusEl = <span className={account.badge_secondary}>Disable</span>;
  }

  return (
    <tr>
      <td>
        <div className={account.flex_center}>
          <input type="checkbox" name="checkbox" checked={props.checked} />
          <div className={account.user}>
            {thumbUsernameEl}
            {props.username}
          </div>
        </div>
      </td>
      <td>{props.phone}</td>
      <td>{props.email}</td>
      <td>{props.email}</td>
      <td>{statusEl}</td>
      <td>
        <img src={moreHorizontal} alt="" />
      </td>
    </tr>
  );
}
