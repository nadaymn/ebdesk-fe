import React from "react";
import { Link } from "react-router-dom";

import Menu from "./menu";
import sidebar from "../../assets/css/sidebar.module.css";

import logo from "../../assets/img/dashboard/logo.svg";
import homeIcon from "../../assets/img/dashboard/home.svg";
import categoryIcon from "../../assets/img/dashboard/category.svg";
import bookmarkIcon from "../../assets/img/dashboard/bookmark.svg";
import profileIcon from "../../assets/img/dashboard/profile.svg";

export default function Sidebar(props) {
  if (props.active == "workspace") {
    return (
      <div className={sidebar.sidebar}>
        <img src={logo} className={sidebar.logo} alt="logo" />
        <ul className={sidebar.menu}>
          <Link to="/dashboard">
            <Menu icon={homeIcon} active={false} />
          </Link>
          <Menu icon={categoryIcon} active={false} />
          <Link to="/workspace">
            <Menu icon={bookmarkIcon} active={true} />
          </Link>
          <Menu icon={profileIcon} active={false} />
        </ul>
      </div>
    );
  } else if (props.active == "dashboard") {
    return (
      <div className={sidebar.sidebar}>
        <img src={logo} className={sidebar.logo} alt="logo" />
        <ul className={sidebar.menu}>
          <Link to="/dashboard">
            <Menu icon={homeIcon} active={true} />
          </Link>
          <Menu icon={categoryIcon} active={false} />
          <Link to="/workspace">
            <Menu icon={bookmarkIcon} active={false} />
          </Link>
          <Menu icon={profileIcon} active={false} />
        </ul>
      </div>
    );
  } else if (props.active == "account") {
    return (
      <div className={sidebar.sidebar}>
        <img src={logo} className={sidebar.logo} alt="logo" />
        <ul className={sidebar.menu}>
          <Link to="/dashboard">
            <Menu icon={homeIcon} active={false} />
          </Link>
          <Menu icon={categoryIcon} active={false} />
          <Link to="/workspace">
            <Menu icon={bookmarkIcon} active={true} />
          </Link>
          <Menu icon={profileIcon} active={false} />
        </ul>
      </div>
    );
  }
}
