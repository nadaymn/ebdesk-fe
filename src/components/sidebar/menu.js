import React from "react";

import sidebar from "../../assets/css/sidebar.module.css";

export default function Menu(props) {
  if (props.active == true) {
    return (
      <>
        <li className={sidebar.active}>
          <img src={props.icon} className={sidebar.menu_item} alt="" />
        </li>
      </>
    );
  }
  return (
    <>
      <li>
        <img src={props.icon} className={sidebar.menu_item} alt="" />
      </li>
    </>
  );
}
