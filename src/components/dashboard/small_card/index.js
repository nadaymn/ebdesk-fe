import React from "react";

import dashboard from "../../../assets/css/dashboard.module.css";

export default function SmallCard(props) {
  return (
    <div className={dashboard.small_card}>
      <div className={dashboard.card_content}>
        <p>{props.title}</p>
        <h5>{props.value}</h5>
      </div>
      <div className={dashboard.card_icon}>
        <span>
          <img src={props.img} alt="" />
        </span>
      </div>
    </div>
  );
}
