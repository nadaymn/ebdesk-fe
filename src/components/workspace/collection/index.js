import React from "react";

import CardItem from "../card";

import workspace from "../../../assets/css/workspace.module.css";
import utility from "../../../assets/css/utility.module.css";

export default function Collection({ children }) {
  return (
    <div className={`${workspace.collection} ${utility.mt_3}`}>{children}</div>
  );
}
