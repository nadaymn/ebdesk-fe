import React from "react";

import workspace from "../../../assets/css/workspace.module.css";
import utility from "../../../assets/css/utility.module.css";

import calendarIcon from "../../../assets/img/workspace/calendar.svg";
import editIcon from "../../../assets/img/workspace/edit.svg";
import trashIcon from "../../../assets/img/workspace/trash.svg";

export default function CardItem(props) {
  return (
    <div className={workspace.card}>
      <div className={workspace.card_header}>
        <img src={props.image} alt="" />
        <h2>{props.title}</h2>
      </div>
      <div className={workspace.hr}></div>
      <div className={workspace.card_body}>
        <div className={workspace.item}>
          <p>{props.desc1}</p>
          <h5>{props.value1}</h5>
        </div>
        <div className={workspace.item}>
          <p>{props.desc2}</p>
          <h5>{props.value2}</h5>
        </div>
      </div>
      <div className={workspace.card_footer}>
        <div className={workspace.item}>
          <img src={calendarIcon} alt="" />
          <p>{props.date}</p>
        </div>
        <div className={workspace.item}>
          <img src={editIcon} alt="" />
          <img src={trashIcon} alt="" />
        </div>
      </div>
    </div>
  );
}
