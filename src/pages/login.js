import React from "react";
import MediaQuery from "react-responsive";

import login from "../assets/css/login.module.css";
import utility from "../assets/css/utility.module.css";
import button from "../assets/css/button.module.css";

import logo from "../assets/img/login/login-logo.svg";
import eyeIcon from "../assets/img/login/eye-icon.svg";
import elipse39 from "../assets/img/login/elipse-39.svg";
import elipse38 from "../assets/img/login/elipse-38.svg";
import loginImg from "../assets/img/login/login-img.svg";
import rectangle486 from "../assets/img/login/rectangle-486.svg";

export default function Login() {
  return (
    <div className={login.wrapper}>
      <div className={login.rectangle}>
        <div className={login.logo}>
          <img src={logo} alt="" />
        </div>
        <img src={elipse39} className={login.elipse_39} alt="" />
        <img src={elipse38} className={login.elipse_38} alt="" />
        <img src={loginImg} className={login.image} alt="" />
        <p className={login.description}>
          Object storage for companies of all sizes. Store any amount of data.
          Retrieve it as often as you’d like.
        </p>
        <div className={login.bar}>
          <img src={rectangle486} alt="" />
        </div>
      </div>
      <img src={loginImg} className={login.image_responsive} alt="" />
      <div className={login.login_form}>
        <div className={login.header}>
          <h2>Nice to see you again</h2>
          <p>Please sign in with your email</p>
        </div>
        <form action="/dashboard">
          <label className={login.label}>Login</label>
          <input
            type="text"
            className={login.input}
            placeholder="Email or phone number"
          />
          <label className={login.label}>Password</label>
          <div className={login.form_input}>
            <input
              type="password"
              className={login.input}
              placeholder="Enter password"
            />
            <img src={eyeIcon} className={login.eye_icon} alt="" />
          </div>
          <div className={login.additional}>
            <label className={login.switch}>
              <input type="checkbox" />
              <span className={`${login.slider} ${login.round}`}></span>
            </label>
            <p>Remember me</p>
            <a>Forgot password?</a>
          </div>
          <button
            className={`${utility.mt_5} ${button.btn} ${button.btn_dark_blue}`}
          >
            Sign In
          </button>
          <hr className={utility.mt_5} />
          <p className={`${utility.mt_5} ${login.copyright_text}`}>
            Ebdesk Technology 2021
          </p>
        </form>
      </div>
      <MediaQuery maxWidth={768}>
        <div className={login.login_form_responsive}>
          <div className={login.header}>
            <h2>Sign In</h2>
            <p>Please enter the details below to continue</p>
          </div>
          <form action="/dashboard">
            <div className={login.form_control}>
              <input type="email" placeholder="Email" />
              <div className={login.input_icon}>
                <input type="password" placeholder="Password" />
                <img src={eyeIcon} className={login.eye_icon} alt="" />
              </div>
              <div className={`${login.input_checkbox} ${utility.mt_2} `}>
                <input type="checkbox" className={login.checkbox} />
                <label>
                  Remember me for keep my data to make the login easier
                </label>
              </div>
            </div>
            <div className={login.footer}>
              <button
                type="submit"
                className={`${button.btn} ${button.btn_dark_blue} ${utility.mt_5} ${utility.mb_3}`}
              >
                Log In
              </button>
              <a>Forgot your password?</a>
            </div>
          </form>
        </div>
      </MediaQuery>
    </div>
  );
}
