import React, { useState } from "react";

import Navbar from "../components/navbar";
import Sidebar from "../components/sidebar";
import Modal from "../components/modal";
import Table from "../components/account/table";

import account from "../assets/css/account.module.css";
import content from "../assets/css/content.module.css";
import button from "../assets/css/button.module.css";
import utility from "../assets/css/utility.module.css";

import searchIcon from "../assets/img/account/search.svg";
import user3 from "../assets/img/workspace/3-user.svg";

const Account = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <div className={account.wrapper}>
        <Sidebar active="account" />
        <Navbar />
        <div className={content.content}>
          <div class={content.header}>
            <h1>Account Management</h1>
            <div class={content.switch_btn}>
              <a href="/account" class={content.active}>
                Account
              </a>
              <a href="workspace">Workspace</a>
            </div>
          </div>
          <div class={`${account.card} ${utility.mt_1} ${utility.mb_5}`}>
            <div class={account.card_header}>
              <div class={account.input_icon}>
                <input
                  type="text"
                  placeholder="Search Name"
                  className={account.custom_search}
                />
                <img src={searchIcon} alt="" />
              </div>
              <div class={account.flex_center}>
                <select class={account.sorting_box}>
                  <option value="" selected>
                    Sort : Descending
                  </option>
                  <option value="">Sort : Ascending</option>
                </select>
                <button
                  className={`${utility.ml_1} ${button.btn_outline_blue}`}
                  onClick={() => setIsOpen(true)}
                >
                  <img src={user3} alt="" />
                  <p class={utility.ml_1}>Create Account</p>
                </button>
              </div>
            </div>
            <Table />
          </div>
        </div>
      </div>
      {isOpen && (
        <Modal setIsOpen={setIsOpen}>
          <label for="">Username</label>
          <input type="text" placeholder="Username" />
          <label for="">Phone Number</label>
          <input type="number" placeholder="ex: 09876565679" />
          <label for="">Email</label>
          <input type="email" placeholder="ex: example@gmail.com" />
          <label for="">Password</label>
          <input type="password" placeholder="Password" />
          <label>Status</label>
          <select id="status">
            <option value="Pilih Status" disabled selected>
              Pilih Status
            </option>
            <option value="Active">Active</option>
            <option value="Disable">Disable</option>
          </select>
        </Modal>
      )}
    </>
  );
};

export default Account;
