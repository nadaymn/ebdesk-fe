import React from "react";

import Navbar from "../components/navbar/index";
import Sidebar from "../components/sidebar/index";
import SmallCard from "../components/dashboard/small_card";

import utility from "../assets/css/utility.module.css";
import dashboard from "../assets/css/dashboard.module.css";

import user2 from "../assets/img/dashboard/small-card/2-user.svg";
import user from "../assets/img/dashboard/small-card/user.svg";
import progress from "../assets/img/dashboard/small-card/progress.svg";
import danger from "../assets/img/dashboard/small-card/danger.svg";
import closeSquare from "../assets/img/dashboard/small-card/close-square.svg";
import tickSquare from "../assets/img/dashboard/small-card/tick-square.svg";
import horizontalElipses from "../assets/img/dashboard/more-horizontal.svg";
import workIcon from "../assets/img/dashboard/frame/work.svg";

export default function Dashboard() {
  return (
    <div className={dashboard.wrapper}>
      <Sidebar active="dashboard" />
      <Navbar />
      <div className={dashboard.content}>
        <h1 className={dashboard.title}>Dashboard</h1>
        <div className={`${dashboard.card_frame} ${utility.mt_3}`}>
          <SmallCard title="All Candidate" value={120} img={user2} />
          <SmallCard title="New Candidate" value={40} img={user} />
          <SmallCard title="On Progress" value={25} img={progress} />
          <SmallCard title="Pending" value={47} img={danger} />
          <SmallCard title="Not Met" value={15} img={closeSquare} />
          <SmallCard title="Completed" value={92} img={tickSquare} />
        </div>
        <div className={`${dashboard.chart_frame} ${utility.mt_3}`}>
          <div className={dashboard.card_1}>
            <div className={` ${dashboard.card_header} ${utility.mt_2}`}>
              <h1>Candidate Overview</h1>
              <div className={dashboard.label}>
                <div className={dashboard.item}>
                  <div className={dashboard.box_dark_blue}></div>
                  <p>New Candidate</p>
                </div>
                <div className={dashboard.item}>
                  <div className={dashboard.box_red}></div>
                  <p>On Progress</p>
                </div>
                <div className={dashboard.item}>
                  <div className={dashboard.box_orange}></div>
                  <p>On Progress</p>
                </div>
                <div className={dashboard.item}>
                  <div className={dashboard.box_blue}></div>
                  <p>On Progress</p>
                </div>
              </div>
              <img src={horizontalElipses} alt="" />
            </div>
          </div>
          <div className={dashboard.card_2}>
            <div className={`${dashboard.card_header} ${utility.mt_2}`}>
              <h1>Pie Chart</h1>
              <img src={horizontalElipses} alt="" />
            </div>
            <div className={`${dashboard.filter} ${utility.mt_1}`}>
              <select>
                <option value="" selected>
                  University
                </option>
              </select>
            </div>
            {/* pe chart */}
            <div className={`${dashboard.label} ${utility.mt_2}`}>
              <div className={dashboard.item}>
                <div className={dashboard.circle_1}></div>
                <p>Chart 1</p>
              </div>
              <div className={dashboard.item}>
                <div className={dashboard.circle_2}></div>
                <p>Chart 2</p>
              </div>
              <div className={dashboard.item}>
                <div className={dashboard.circle_3}></div>
                <p>Chart 3</p>
              </div>
            </div>
          </div>
        </div>
        <div
          className={`${dashboard.chart_frame_2} ${utility.mt_3} ${utility.mb_3}`}
        >
          <div className={dashboard.card_full}>
            <div className={dashboard.card_header}>
              <h1>Need vs Met</h1>
              <img src={horizontalElipses} alt="" />
            </div>
            <div
              className={`${dashboard.card_content} ${utility.mt_3} ${utility.mb_5}`}
            >
              <div className={dashboard.chart_wrapper}>
                <div className={dashboard.middle_text}>
                  <h1>50 %</h1>
                  <p>Back End</p>
                </div>
                {/* backend */}
              </div>
              <div className={dashboard.chart_wrapper}>
                <div className={dashboard.middle_text}>
                  <h1>25 %</h1>
                  <p>Marketing</p>
                </div>
                {/* marketing */}
              </div>
              <div className={dashboard.chart_wrapper}>
                <div className={dashboard.middle_text}>
                  <h1>75 %</h1>
                  <p>Front End</p>
                </div>
                {/* front end */}
              </div>
              <div className={dashboard.chart_wrapper}>
                <div className={dashboard.middle_text}>
                  <h1>100 %</h1>
                  <p>Data Analyst</p>
                </div>
                {/* data analys */}
              </div>
              <div className={dashboard.vertical_line}></div>
              <div className={dashboard.frame_1}>
                <div className={dashboard.detail}>
                  <div className={dashboard.icon}>
                    <img src={workIcon} alt="" />
                  </div>
                  <div className={dashboard.desc}>
                    <p>Opening Job</p>
                    <h1>120</h1>
                  </div>
                </div>
                <div className={dashboard.detail}>
                  <div className={dashboard.icon}>
                    <img src={workIcon} alt="" />
                  </div>
                  <div className={dashboard.desc}>
                    <p>Work Candiate Average</p>
                    <h1>1 Years</h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
