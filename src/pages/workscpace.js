import React, { useState } from "react";

import Navbar from "../components/navbar";
import Sidebar from "../components/sidebar";
import Modal from "../components/modal/index";
import Collection from "../components/workspace/collection";
import CardItem from "../components/workspace/card";

import workspace from "../assets/css/workspace.module.css";
import content from "../assets/css/content.module.css";
import button from "../assets/css/button.module.css";
import utility from "../assets/css/utility.module.css";

import user3 from "../assets/img/workspace/3-user.svg";
import arrowRight from "../assets/img/account/arrow-right.svg";
import arrowLeft from "../assets/img/account/arrow-left.svg";

import googleIcon from "../assets/img/workspace/card/google.svg";
import netflixIcon from "../assets/img/workspace/card/netflix.svg";
import gojekIcon from "../assets/img/workspace/card/gojek.svg";
import paypalIcon from "../assets/img/workspace/card/paypal.svg";
import grabIcon from "../assets/img/workspace/card/grab.svg";
import teslaIcon from "../assets/img/workspace/card/tesla.svg";

const Workspace = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <div className={workspace.wrapper}>
        <Sidebar active="workspace" />
        <Navbar />
        <div className={content.content}>
          <div className={content.header}>
            <h1>Workspace</h1>
            <div className={content.right_control}>
              <button
                type="button"
                onClick={() => setIsOpen(true)}
                className={button.btn_outline_blue}
              >
                <img src={user3} alt="" />
                <p className={utility.ml_1}>Create Workspace</p>
              </button>
              <div className={`${utility.ml_1} ${content.switch_btn}`}>
                <a href="/account">Account</a>
                <a href="/workspace" className={content.active}>
                  Workspace
                </a>
              </div>
            </div>
          </div>

          <Collection>
            <CardItem
              image={googleIcon}
              title="Google Company"
              desc1="Max Account"
              value1="21 / 21"
              desc2="Max Assesment"
              value2="21 / 21"
              date="21-04-2021"
            />
            <CardItem
              image={netflixIcon}
              title="Netflix"
              desc1="Max Account"
              value1="21 / 21"
              desc2="Max Assesment"
              value2="21 / 21"
              date="21-04-2021"
            />
            <CardItem
              image={gojekIcon}
              title="Gojek"
              desc1="Max Account"
              value1="21 / 21"
              desc2="Max Assesment"
              value2="21 / 21"
              date="21-04-2021"
            />
            <CardItem
              image={paypalIcon}
              title="Paypal"
              desc1="Max Account"
              value1="21 / 21"
              desc2="Max Assesment"
              value2="21 / 21"
              date="21-04-2021"
            />
          </Collection>

          <Collection>
            <CardItem
              image={grabIcon}
              title="Grab"
              desc1="Max Account"
              value1="21 / 21"
              desc2="Max Assesment"
              value2="21 / 21"
              date="21-04-2021"
            />
            <CardItem
              image={googleIcon}
              title="Google Company"
              desc1="Max Account"
              value1="21 / 21"
              desc2="Max Assesment"
              value2="21 / 21"
              date="21-04-2021"
            />
            <CardItem
              image={teslaIcon}
              title="Tesla"
              desc1="Max Account"
              value1="21 / 21"
              desc2="Max Assesment"
              value2="21 / 21"
              date="21-04-2021"
            />
            <CardItem
              image={paypalIcon}
              title="Paypal"
              desc1="Max Account"
              value1="21 / 21"
              desc2="Max Assesment"
              value2="21 / 21"
              date="21-04-2021"
            />
          </Collection>

          <Collection>
            <CardItem
              image={googleIcon}
              title="Google Company"
              desc1="Max Account"
              value1="21 / 21"
              desc2="Max Assesment"
              value2="21 / 21"
              date="21-04-2021"
            />
            <CardItem
              image={paypalIcon}
              title="Paypal"
              desc1="Max Account"
              value1="21 / 21"
              desc2="Max Assesment"
              value2="21 / 21"
              date="21-04-2021"
            />
            <CardItem
              image={grabIcon}
              title="Grab"
              desc1="Max Account"
              value1="21 / 21"
              desc2="Max Assesment"
              value2="21 / 21"
              date="21-04-2021"
            />
            <CardItem
              image={teslaIcon}
              title="Tesla"
              desc1="Max Account"
              value1="21 / 21"
              desc2="Max Assesment"
              value2="21 / 21"
              date="21-04-2021"
            />
          </Collection>

          <div className={`${content.footer} ${utility.mb_5} ${utility.mt_5}`}>
            <div className={button.pagination}>
              <button>
                <img src={arrowLeft} alt="" />
              </button>
              <button className={button.active}>1</button>
              <button>2</button>
              <button>3</button>
              <button>4</button>
              <button>
                <img src={arrowRight} alt="" />
              </button>
            </div>
          </div>
        </div>
      </div>
      {isOpen && (
        <Modal setIsOpen={setIsOpen}>
          <label htmlFor="">Company Name</label>
          <input type="text" placeholder="Company Name" />
          <label htmlFor="">Max Account</label>
          <input type="number" placeholder="ex: 100" />
          <label htmlFor="">Max Assesment</label>
          <input type="number" placeholder="ex: 35" />
        </Modal>
      )}
    </>
  );
};

export default Workspace;
