import "./assets/css/global.css";
import { Component } from "react";
import MyRoutes from "./routes/routes";

class App extends Component {
  render() {
    return <MyRoutes />;
  }
}

export default App;
