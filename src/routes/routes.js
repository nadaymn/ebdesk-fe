import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Login from "../pages/login";
import Dashboard from "../pages/dashboard";
import Workscpace from "../pages/workscpace";
import Account from "../pages/account";
import Tes from "../components/sidebar/index";

export default function MyRoutes() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Login />}></Route>
        <Route exact path="/dashboard" element={<Dashboard />}></Route>
        <Route exact path="/workspace" element={<Workscpace />}></Route>
        <Route exact path="/account" element={<Account />}></Route>
        <Route exact path="/tes" element={<Tes />}></Route>
      </Routes>
    </BrowserRouter>
  );
}
